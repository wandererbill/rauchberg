use Test::More;
use File::Slurper qw(read_lines read_dir);
for my $file (read_dir('.')) {
  next unless -f $file;
  next unless $file =~ /\.md$/;
  next if $file eq "Rauchberg.md";
  my @lines = read_lines($file);
  my $n = 0;
  my $err = 0;
  for (@lines) {
    $n++;
    next unless /[,.!?]“\./;
    $err++;
    ok(0, "$file:$n: $_");
  }
  ok(1, "zitat: $file") if $err == 0;
}
done_testing;
