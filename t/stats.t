use Test::More;
use File::Slurper qw(read_text read_dir);
for my $file (read_dir('.')) {
  next unless -f $file;
  next unless $file =~ /\.md$/;
  next if $file eq "Rauchberg.md";
  my $text = read_text($file);
  my $ok = 1;
  while ($text =~ /\b(TW\s+\S+\s+(?:TP\s+\S+\s+)?(\S+)\s+\S+\s+\S+\s+(\S+))/g) {
    if ($2 ne "RK") { warn "$file: $1 (RK is $2)\n"; $ok = 0; }
    if ($3 !~ /^[KM]/) { warn "$file: $1 (save is $3)\n"; $ok = 0; }
  }
  ok($ok, $file);
}
done_testing;
