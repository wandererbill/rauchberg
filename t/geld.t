use Test::More;
use File::Slurper qw(read_lines read_dir);
for my $file (read_dir('.')) {
  next unless -f $file;
  next unless $file =~ /\.md$/;
  next if $file eq "Rauchberg.md";
  my @lines = read_lines($file);
  my $n = 0;
  my $err = 0;
  for (@lines) {
    $n++;
    next unless /((\d)+[.,]?(\d\d\d))/;
    next if $1 > 2020 and $1 < 2025;
    $err++;
    ok(0, "$file:$n: $_");
  }
  ok(1, "geld: $file") if $err == 0;
}
done_testing;
