## Blut Tempel

![](03-1-Blut-Tempel.png)

| 1W6 | Monster            |
|:---:|--------------------|
| 1   | 1W6 Akolyten       |
| 2   | 1W6 Goblinspione   |
| 3   | 1W6 Hyänenmenschen |
| 4   | Salka der Blutling |
| 5   | Osk der Ghul       |
| 6   | Ástarbrími         |

**Akolyten** TW 1 RK 9 1W6 K1 BW 12 ML 6 EP 100; *brennende Hände* 1W6
gegen alle innerhalb von 10 Fuss; Dolch. („Helft uns gegen die
Aasfresser!“)

**Goblinspione** TW 1-1 RK 6 1W6 K1 BW 6 ML 7 EP 100; versteckte
Langmesser. („Bitte tut uns nichts, wir haben mit all dem nichts zu
tun…“)

**Hyänenmenschen** TW 2+1 RK 5 1W10 K2 BW 9 ML 8 EP 200; Kette,
Zweihänder, Dolch. (Falls 1: „Ich bin Salkas Beschützer!“ sonst aber:
„der Frevel muss beendet werden!“)

**Salka der Blutling** TW 3 TP 24 RK 9 1W6 K3 ML 10 EP 300;
*Bezauberung* für 1 Tag oder Rettungswurf gegen Sprüche, 2×/Tag;
*Aderfeuer* innerhalb von 10 Fuss für 1W6 Schaden für 3 Runden, 1×/Tag
oder Rettungswurf gegen Sprüche. („Ihr müsst den Ghul Osk
beseitigen!“)

**Osk der Ghul** TW 2 RK 6 1d4/1d4/1d4 K2 BW 9 ML 9 XP 200; *Lähmung*
für 1h oder Rettungswurf gegen Lähmung; **Aura der Angst** gegen eine
Person innerhalb von 30 Fuss: Flucht für 2 Runden oder Rettungswurf;
gegen Sprüche; **Hyänengestalt** nach belieben. In Menschengestalt
trägt Osk die Kleidung eines Kapitäns: Lederstiefel, Mantel mit
goldenen Knöpfen, breiter Hut mit Silbertotenkopf. („Die Blutlinge
müssen gestoppt werden!“)

**Ástarbrími der Feuersalamander** TW 8 RK 2 1d4/1d4/1d8 K8 BW 12 ML 8
XP 800; *Hitze* verursacht 1W8/Runde innerhalb von 20 Fuss; immun
gegen *Schlaf*, *Bezauberung* und andere Zauber, die auf den Geist
wirken. („Ich bin ein Bote aus Muspelheim und bringe die frohe
Nachricht, das Jötunheim bald fallen wird!“)

<!-- ### Allgemeines -->

<!-- Der Bluttempel wurde in den Vulkankrater gebaut und bewacht die erste -->
<!-- Brücke.  -->

<!-- Der Blutling Salka gebietet hier über Akolyten und -->
<!-- Tempelwächter. Salka will Goblinspione in die Gräberstadt schicken und -->
<!-- hat Hyänenmenschen angeheuert, die ihnen folgen sollen, doch deren -->
<!-- Loyalität ist nicht hoch. Gleichzeitig ist das fliegende Schiff von -->
<!-- Elfen beschädigt worden, und Ghule aus der Gräberstadt sind schon -->
<!-- dabei, den Bluttempel zu unterwandern. -->

### Raumbeschreibung

 1. **Dachkuppel**. Runder Raum, Wendeltreppe hinab (→ #3) offene
    Fenster, das Brodeln der Lava tief unten ist laut, das Licht rot.
    Hier lebt **Ástarbrími der Feuersalamander**, eine über dem Boden
    schwebende Feuergestalt. Eine Krone der Afarit (1’500 Gold) und zwei
    Armbänder aus lebendem Gold (2×1’000 Gold). Ein dunkelgrauer Trank
    der *Kälteresisten* (1h). **Kiste**: 5’000 Silber, 300 Platin.

 2. **Dachterrasse**. Wilder Wind von unten. Blick in die Tiefe. Unten
    auf der Plattform (30 Fuss weiter unten → #12) ist ein
    Astralschiff gelandet.

 3. **Laboratorium**. Runder Raum, Wendeltreppe hoch (→ #1) und
    hinunter (→ #5), offene Fenster, lautes Brodeln von draussen,
    rotes Licht. Zwei gläserne Blutlingssärge, mit Schläuchen und
    dutzenden von Nadeln, leer. Sind zwei Leute eingespannt, kann
    einer mittels Bluttransfusion neues, verdorbenes Leben erhalten:
    1W6 Gesundheitpunkte (Konstitution) können so für eine Woche
    transferiert werden (Maximum 18, bei 0 stirbt man). Am Tisch
    verhandeln 4 **Akolyten** und **Salka der Blutling** mit 6
    **Goblinspione**. („Für den Kopf von Hrægífr von der Gräberstadt
    zahle ich 2’000 Gold.“) Im Hintergrund eine Hängematte und eine
    **Kiste**: 4’000 Silber, 3’000 Gold, 400 Platin, Krone des
    Hohepriesters (1’200 Gold), goldener Kelch (200 Gold), goldene
    Ringe (2× 100 Gold), grosser Silberdoch (30 Gold), kleiner
    Silberdoch (20 Gold).

 4. **Eingang**. Links und rechts stehen Statuen von Afarit mit
    Hellebarden. Inschrift „Nützen der Eltern Erklärungen nicht, so
    schützen dich die zwei Boten des Sutr: Drohen und Gewalt“. Auf den
    Treppen liegt ein Toter Akolyt, von hinten mit einem Schwert
    erschlagen. Nach Osten führt ein Tunnel zum Sutr Tempel. Siehe
    *Sutr Tempel, Untergeschoss* #3).

 5. **Grosses Treppenhaus**. Wendeltreppe führt hoch (→ #3) und
    hinunter (→ untere Hälfte, #1). 2 **Hyänen** (Ghule in Tierform),
    warten ab und beobachten.

 6. **Rechte Schlafräume**. Leer.

 7. **Rechter Geschützturm**. Schiessscharten auf Plattform und das
    rechte Katapult.

 8. **Verteidigungskammer**. Schiessscharten auf Plattform. 6 nervöse
    **Tempelwächter**: TW 1 RK 5 1W6 K1 BW 9 ML 7 EP 100; Kette,
    Armbrust, Dolch. („Vor den Elfen muss man sich in acht nehmen!“)

 9. **Plattform**. Eine Delegation Elfen diskutieren mit 4
    **Akolyten**, die versuchen, Zeit zu gewinnen. („Salka ist im
    Moment beschäftigt, habt Geduld.“) **Elfen**: TW 1 RK 5 1W6 E1
    BW 9 ML 8 EP 100; Kette, Bogen, Schwert; *Schlaf* 2W8 TW. („Wir
    wurden vom Katapult beschossen!“) **Gwauril der Geradlinige**:
    TW 5 RK 3 1W6 BW 9 ML 8 EP 500; Kette, Bogen, Schwert; *Schlaf*
    2W8 TW; *Wasserwandel* 20min; *Auge* 10min; *Flammenschwert* +1W6,
    20min; *Atemluft* 1 Tag. („Ich verlange Schadenersatz – bringt
    mich zu Salka!“)

10. **Rechtes Katapult**. Ein verängstigte **Tempelwächter** am
    Katapult: TW 1 RK 5 1W6 K1 BW 9 ML 7 EP 100; Kette, Armbrust,
    Dolch. („Es war ein Unfall!“)

11. **Linkes Katapult**. Leer.

12. **Landeplattform**. Freier Blick auf die Brücke weiter unten. Ein
    Astralschiff steht auf der Plattform, stark beschädigt. **Tobaor
    der Kapitän** begutachtet den Schaden: TW 5 RK 3 1W6 BW 9 ML 8
    EP 500; Kette, Bogen, *Schwert* +1 mit den Runen des elfischen
    Schmieds Mistaor; *Schlaf* 2W8 TW; *Wasserwandel* 20min; *Auge*
    10min; *Flammenschwert* +1W6, 20min; *Atemluft* 1 Tag. („Das
    Schiff wird so schnell nicht wieder fliegen. Wir werden eine Weile
    hier bleiben müssen.“)

13. **Linke Schlafräume**. 4 schlafende **Tempelwächter**: TW 1 RK 9
    1W6 K1 BW 9 ML 7 EP 100; Dolch. („Was? Huh?“) 4× Kette, 4×
    Armbrust.

14. **Rechter Geschützturm**. Schiessscharten auf Plattform und das
    linke Katapult.

15. **Unteres Treppenhaus**. Drei Türen. Im Südwesten hat es ein Bild
    eines Vulkankraters mit drei Brücken und einem roten Drachen am
    Boden → Dachenkopf ragt etwas hervor → druck entriegelt die
    Geheimtüre.

16. **Räume der Hyänenmenschen**. Eine **Hyäne** wacht hier: TW 2+1
    RK 7 1d6 K1 BW 18 ML 6 XP 200. („Grrrrr!“) 6 **Hyänenmenschen**
    würfeln, Schwerter griffbereit. („Starr mich nicht so an,
    Affengesicht.“) **Mylya**: TW 4+1 RK 3 1W10 K4 BW 6 ML 8 EP 400.
    („Besprecht keine Geheimnisse vor Hyänen.“) **Kiste**: 5’000
    Silber.

17. **Brückenmodell**. Schmale Fenster im Norden. Ein Bronzemodell der
	ersten Brücke. Eine sachkundige Person kann erkennen, dass es in
	der Brücke selber noch einen Gang geben muss.

18. **Tempel des Bálbrú**. Statue eines Dämons mit Hörnern und
	Schwingen, der auf einem dünnen Grat läuft. Fenster an der Nord
	und Westwand, Feuermalereien an Südwand → Feuermenschen, Hydras,
	Skorpione, und ein roter Drachen → Dachenkopf ragt etwas hervor →
	druck entriegelt die Geheimtüre.

19. **Ghul Lager**. Bett und Kiste der Ghule. Chance von 1–5/6, dass
	**Osk der Ghul** hier ist. **Kiste** mit Kombinationsschloss und
	giftigen Nadeln. Wer nichts davon versteht vergiftet sich und
	stirbt in 30min oder Rettungswurf gegen Gift. In der Kiste hat es
	diverse Verkleidungstücke sowie sechs goldeneTotenmasken der
	Harmdauđi Priester mit dem Nergalzeichen (6× 1’000 Gold).
	**Teppich** (200 Gold) verdeckt Falltüre nach unten (→ #25).

20. **Geheime Treppe** führt nach unten (→ #27).

21. **Brückengeschoss**. Das untere Ende der grossen Wendeltreppe. Das
	Tor zur Brücke im Westen ist dreifach verriegelt.

22. **Erste Brücke**. Hinaus in die Leere, unten die kochende Lava,
	eine Brücke, fast 20 Fuss breit, die nach Westen zur Gräberstadt
	führt. Am östlichen Ende hat es Spiesse im Boden, welche einen
	Sturm verhindern sollen.

23. **Wachraum**. 2 **Hyänenmenschen** diskutieren den Auftrag, die
	Gräberstadt zu erobern.

24. **Schlafraum der Wachen**. Einfache Betten. 4 **Hyänenmenschen**
	am ruhen; im Moment ungerüstet (RK 9). Schwerter griffbereit.

25. **Geheimer Wachraum**. Leer. Es gibt eine direkte Verbindung zum
	Ruheraum der Ghule (→ #26). Ghule haben scharfe Ohren.

26. **Ruheraum der Ghule**. Schreibtisch, Stühle. 6 **Ghule**: TW 2
	RK 6 1d4/1d4/1d4 K2 BW 9 ML 9 XP 200; *Lähmung* für 1h oder
	Rettungswurf gegen Lähmung; **Aura der Angst** gegen eine Person
	innerhalb von 30 Fuss: Flucht für 2 Runden oder Rettungswurf;
	gegen Sprüche; **Hyänengestalt** nach belieben.

27. **Zwischengeschoss**. Leer. Treppe nach oben (Osten → #20) und
    nach unten (Westen → #28)

28. **Geheimer Brückengang**. In der Brücke gibt es einen Gang, der
    ebenfalls nach Westen zur Gräberstadt führt.
