In diesem Repositorium wird der Rauchberg Megadungeon geschrieben. Die
generierten PDFs findet man unter anderem [auf der Webseite von Alex
Schroeder](https://alexschroeder.ch/pdfs/rauchberg/).

## Werkzeuge

Ich verwende Pythons [Markdown](https://pypi.org/project/Markdown/)
Prozessor um die Markdown Dateien zu HTML zu konvertieren, und dann
verwende ich [WeasyPrint](https://pypi.org/project/WeasyPrint/) um
HTML zu PDF zu konvertieren.

```sh
sudo apt install make python3 python3-pip python3-markdown
pip3 install weasyprint
```

## Schriftarten

In der CSS Datei werden "fbb" oder "Palatino" für den Text verwendet.
Die fbb Schriftart stammt aus dem Debian Packet `texlive-fonts-extra`.
Das Problem ist, dass `pango` diese Schriften nicht automatisch
findet. Die einfachste Lösung waren symbolische Links von
`/usr/share/texlive/texmf-dist/fonts/opentype/public/fbb/` zu
`~/.local/share/fonts/`.

Die Titelseite verwendet "Gill Sans", "Gillus ADF" oder "Helvetica".
Erstaunlicherweise wird bei mir allerdings TeX Gyre Heros gewählt (ein
Ersatz für Helvetica, basierend auf URW Nimbus Mono L). Die findet man
im Debian Packet `fonts-texgyre`.

```sh
sudo apt install texlive-fonts-extra fonts-texgyre
ln -s /usr/share/texlive/texmf-dist/fonts/opentype/public/fbb \
  ~/.local/share/fonts/
```
