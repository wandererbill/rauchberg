## Ghullande, unter der Brücke

![](04-2-Ghullande.png)

| 1W6 | Monster                    |
|:---:|----------------------------|
| 1   | 2W6 Skelette               |
| 2   | 1W6 Zombies                |
| 3   | 1W6 Goblinspione           |
| 4   | 1W6 Hyänenmenschen         |
| 5   | 1W6 Ghule                  |
| 6   | falls gelb: 1 Knochengolem |
|     | ansonsten: Edelghul Svana  |

**Skelette** TW 1 RK 7 1W6 K1 BW 6 ML 7 EP 100; immun gegen *Schlaf*,
*Bezauberung* und andere Zauber, die auf den Geist wirken.

**Goblinspione** TW 1-1 RK 6 1W6 K1 BW 6 ML 7 EP 100; versteckte
Langmesser. („Wir suchen den Knochenmagier Etzel.“)

**Hyänenmenschen** TW 2+1 RK 5 1W10 K2 BW 9 ML 8 EP 200; Kette,
Zweihänder, Dolch. („Unsere Bande wurde zerschlagen und Gagarr
gefangen genommen.“)

**Ghule** TW 2 RK 6 1d4/1d4/1d4 K2 BW 9 ML 9 XP 200; *Lähmung* für 1h
oder Rettungswurf gegen Lähmung; **Aura der Angst** gegen eine Person
innerhalb von 30 Fuss: Flucht für 2 Runden oder Rettungswurf; gegen
Sprüche; **Hyänengestalt** nach belieben. („Frischfleisch! Seid ihr
schon reserviert?“)

**Edelghul Svana** TW 5 RK 6 1d4/1d4/1d4 K2 BW 9 ML 9 XP 500;
*Lähmung* für 1h oder Rettungswurf gegen Lähmung; **Aura der Angst**
gegen eine Person innerhalb von 30 Fuss: Flucht für 2 Runden oder
Rettungswurf; gegen Sprüche; **Hyänengestalt** nach belieben.
(„Seid ihr neu hier?“)

**Knochengolem** TW 8 RK 2 1d6/1d6/1d6/1d6 K16 BW 12 ML 12 XP 800;
*immun gegen Pfeile*; *immun gegen Gift*; *immun gegen Sprüche*; wird
ein Golem zerlegt, findet man in der Lebensmatrix 2W6 Edelsteine 1W20:
1–3 → 10, 4–6 → 20, 7–9 → 50, 10–12 → 75, 13–15 → 100, 16–17 → 250,
18–19 → 750, 20 → 1000 Gold.

### Raumbeschreibung

 1. **…**.

 2. **…**.

 3. **…**.

 4. **…**.

 5. **…**.

 6. **…**.

 7. **…**.

 8. **…**.

 9. **…**.

10. **…**.

11. **…**.

12. **…**.

13. **…**.

14. **Beinkammer**. Wände aus Knochen und Schädeln. Einer der Schädel
    hat ein kleines rundes Loch in der Stirn. Dort drinnen hat es
    einen Schalter. **Geheimtüre**: Ist der Schalter betätigt, kann
    man ein paar Knochen zur Seite drücken und sich in einen Gang
    zwängen. **Falle**: An der Ecke hat es vier Löcher in der Wand.
    Wer daran vorbeigeht, löst mit 1–2/6 eine Armbrustbolzenfalle aus:
    4 Schüsse wie ein TW 1 Monster, je 1W6 Schaden.

15. **Altar des Orcus**. Wächterstatuen mit riesigen Hellebarden
    entlang der Wände. **Statue**: Ein dicker Mann mit den Hörnern
    eines Widders lümmelt auf einem steinernen Thron, umringt von
    tanzenden Skeletten. **Altar Inschrift**: „Erhöre mich, Herr des
    ewigen Lebens! Ich weihe Dir die Seele meines Opfers, doch schenke
    uns noch ein paar Jahre!“ Hier kann jeder, der in den letzten 10
    Tagen gestorben ist, und dessen Seele noch niemandem versprochen
    ist, ein letztes Mal wiederbelebt werden.

16. **…**.

17. **Altar der Hel**. Grosser Altar mit eingelassenem Schwert →
    Silberverzierung zeigt einen mehrköpfigen Hund → magische Aura.
    Dahinter die Statue von Hel der Gerechten, Waage in der Hand. Zu
    ihrer Rechten der Recke *Greipr*. **Inschrift**: „Jeden lass’ ich
    passieren, keinen lass’ ich zurück.“ → Lösungswort: „Garm.“ Wer
    das Wort spricht, kann das Schwert nehmen.

	> Das **Schwert Garm** +1/+3 gegen Menschen wurde vom Trollkönig
	> Athanaric im Gletscherkrieg geschmiedet.

18. **…**.

19. **…**.

20. **…**.

21. **…**.

22. **…**.

23. **…**.

24. **…**.

25. **…**.

26. **…**.

27. **…**.

28. **…**.

29. **…**.

30. **…**.

31. **…**.

32. **…**.

33. **Mausoleum von Vigdís**. Statue einer Frau in voller Rüstung.
    **Inschrift**: „Der Höllenhund liess mich passieren, doch den Weg
    zurück ist mir verwehrt.“ Familiengräber unter allen Steinplatten.

34. **Statue des Orcus**. Dick und haarig, mit den Hörnern eines
    Widders, sitzt er auf seinem Thron aus schwarzem Stein, umringt
    von Knochenmännern. **Inschrift**: „Orcus, Herr der Geheimnisse“.
    **Geheimtüre** öffnet sich, wenn man einem Knochenmann links in
    die Augenhöhlen greift und den Schädel dreht.

35. **Zelle**. Hier wird der **Hyänenmensch Gagarr** gefangen
    gehalten. Die Türe ist von aussen dreifach verriegelt.

36. **Zwei Schatzkammern**. Im Westen zwei Kisten mit **Schlange**:
    Beide mit übler Aura → wer den unverschlossenen Deckel öffnet,
    provoziert eine versteckte Ätherschlange → Rettungswurf gegen Tod
    oder *Schwächefluch* (Kraft 3). Im Osten zwei Kisten mit
    **Totenkopf**: verschlossen → total 3000 Silber, 4000 Gold,
    *Minotaurenstab*.
	
	> Der **Minotaurenstab** beschwört 1×/Tag den Stiermenschen
	> **Öldungr**, der immer einen Rat hat. TW 6 RK 6 2d6 F6 BW 12;
	> *hypnotisierende Rede* bewegt alle, Feinseeligkeiten
	> einzustellen und nichts als die Wahrheit zu sagen; immun gegen
	> *Schlaf*, *Bezauberung* und andere Zauber, die auf den Geist
	> wirken. Der Stab verliert seine Wirkung, wenn der Minotaur
	> stirbt.

37. **…**.

38. **…**.

39. **…**.

40. **…**.

