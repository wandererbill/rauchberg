## Sutr Tempel, Unterwelt

![](01-4-Sutr-Tempel.png)

| 1W6 | Monster             |
|:---:|---------------------|
| 1   | Feuerschnecke       |
| 2   | Rote Rauchschlange  |
| 3–4 | 1W6 Goblinspione    |
| 5   | Ylfa und die Katze  |
| 6   | Ulfr der Höllenhund |

**Feuerschnecke** TW 2 RK 8 – K4 BW 1 ML 6 EP 200; immun gegen Feuer; kriecht
sehr langsam dem Boden entlang; sehr breit und blockiert Gänge, wenn
möglich; *automatischer Feuerschaden* 1W8/Runde bei Kontakt oder im
Nahkampf. („Die Lava scheint euch zu folgen.“)

**Rote Rauchschlange** TW 3 RK 9 – K6 BW 3 ML 4 EP 300; kriecht langsam der
Decke entlang; *automatische Blutungen* 1W6/Runde bei Kontakt oder im
Nahkampf. („Tausend Nadelstiche!“)

**Goblinspione** TW 1-1 RK 6 1W6 K1 BW 6 ML 7 EP 100; kleine Armbrüste und
lange Dolche bewaffnet. („Wir dienen Ylfa der Jägerin.“)

**Ylfa und der Unterberglöwe** TW 2 RK 6 1W6+1 K2 BW 6 ML 7 EP 200;
Überraschung 1–4/6; *goblinische Mörderarmbrust* +1 und langer Dolch,
mit Unterberglöwe HD 3 RK 6 1d6 F1 BW 24 ML 7 XP 300. („Lasst mich in
Ruhe und ich lasse euch in Ruhe!“)

**Ulfr der Höllenhund** TW 5 RK 4 1W6 ML 9 EP 500; jeder Angriff hat
eine 1–2/6 Chance, *Flamenatem* für 5W6, Rettungswurf gegen
Drachenodem für die Hälfte; immun gegen Feuer; sieht unsichtbares.
(„Ich werde dich töten, sterbliches Fleisch!“)
	
### Raumbeschreibung

 1. **Höhlen**. Nach Süden hin gibt es Höhlen in den nackten Fels. Die
    Wände sind warm.
	
 2. **Goblinpfade**. Eine schmale Treppe führt in die Finsternis.
    Goblinzeichen: „In zwei Stunden bist du daheim!“
	
 3. **Wachposten**. 3 **Goblinspione** halten hier wache. Von Westen
    hört man ständiges Gebrabbel von Goblins.
	
 4. **Feuerwand**. Unerträglich heiss. Flüssiges Feuer fliesst hier
    zäh von einem oberen Becken herab. Siehe *Sutr Tempel,
    Untergeschoss* #2. Hinter der Lava sieht man einen Durchgang. Wer
    einen Schild opfert oder eine allgemeine Probe besteht (1–6 oder
    Diebesfertigkeiten), kann der Lava ausweichen (1W8).
	
 5. **Hauptquartier**. 10 **Goblinspione** an einer Lagebesprechung
    („die Blutling sind Zauberer, nehmt euch in acht!“). Im Kampf
    öffnen sie den Spinnenstall! Chance von 1–5/6, dass **Ylfa** und
    der **Unterberglöwe** hier sind. **Kiste**: 6’000 Gold. („Haben
    wir aus den Vorratskammern!“)
	
 6. **Spinnenstall**. Die Goblinspione sind auch Spinnenreiter; die
    Gittertüre ist zugebunden, lässt sich aber im Notfall sehr leicht
    öffnen. („Lasst die Spinnen raus!!“) 2 **Riesenspinnen**: TW 4
    RK 6 1d6 K2 ML 7 XP 400; *klettern*; *Lähmungsgift*: Rettungswurf
    oder 20 min Krämpfe; schnell.
	
 7. **Goblinlager**. 10 **Goblinspione** am ausruhen.
 
 8. **Aschekammer**. Die Asche und Knochenreste von hunderten von Urnen.
 
 9. **Rückseite**. Unerträglich heiss. Flüssiges Feuer fliesst hier
    zäh herab. Siehe *Sutr Tempel, Untergeschoss* #2. Hinter der Lava
    sieht man einen Raum. Wer einen Schild opfert oder eine allgemeine
    Probe besteht (1–6 oder Diebesfertigkeiten), kann der Lava
    ausweichen (1W8).

10. **Teleportationszirkel**. Rot glühender Kreis mit Feuerrunen.
    („Erhöre mich, oh Prinz der Asche: Ich bin dein ewiger Diener!“)
    Wer den Kreis betritt und die richten Worte spricht, wird nach
    Muspelheim teleportiert und nimmt dort jede Runde 1W6
    Feuerschaden. Der Spruch für die Rückkehr ist dort ebenfalls in
    Feuerrunen zu lesen. („Ich gehe nun, oh Herr, doch kehre ich
    zurück!“) Siehe *Muspelheim* #?.
 
11. **Höhlensystem**. Schwefelgeruch und Rauch. Sichtweite 10 Fuss.
    Man hört den Höllenhund atmen.

12. **Hundelager**. Hier ist das Lager von Ulfr dem Höllenhund.
    Ständiges Pfeifen von Gasen.

13. **Verriegelte Türe**. Eine von aussen verriegelte Türe führt in
    die Fallgrube. Das Eintreten oder Einschlagen der Türe weckt die
    Aufmerksamkeit von Ulfr dem Höllenhund.

14. **Thron über die Feuerriesen**. Verziert mit stützenden
    Feuerriesen. An den Wänden reichhaltige Verzierungen mit farbigem
    Glas. Bild (100 Gold) „Der Bau einer Stadt im Feuer“ → die
    Feuerriesen sehen unglücklich aus; grosse Feuerschale aus Messing
    (50 Gold).

15. **Statue des Lebens**. Gehörnter Mann im Urschleim. Zu seinen
    Füssen wachsen Pflanzen und Tiere kriechen an Land. Wer ihn
    berührt und um Gnade anfleht, dem wird neues Leben geschenkt, dem
    wird die Gesundheit wieder hergestellt (siehe #16) und die Haut
    wird schwarz wie Russ.

16. **Halle der Riesen**. Im Eingangsbereich hat es Wandmalereien →
	überall sieht man Menschen, die sich gegenseitig grüssen,
	bewirten, beherbergen, und die Götter verehren und ehrfürchtig
	anrufen. Inschrift links: „Gastfreundschaft und Ehre.“ Inschrift
	rechts: „Grüssen und Schenken.“ Die Riesen blicken die
	Eintretenden fragend an. Es gibt hier keine Ablagen, wo man etwas
	deponieren könnte. Auf halbem hat es am Boden die Skulptur eines
	an alterschwäche sterbenden Kriegers. Das Schwert ist ihm aus der
	Hand gefallen und er fleht die Götter um Gnade an. Wer das Buch
	der mächtigsten Riesen im Archiv studiert hat, erkennt die Riesen
	und kann sie grüssen: Ægir vom Meer, Hel von der Unterwelt, Mímir
	von der Geisterwelt, Narfi von der Nacht, Skaði von den Bergen,
	Vafþrúðnir von den Rätseln, Vörnir vom Himmel, Ymir vom Eis. Wer
	das nicht kann, wird von den Göttern bestraft und verliert für
	jeden geschmähten Gott einen Gesundheitspunkt (Konstitution).

17. **Verlassenes Wachzimmer**. Leer.
<span style="display: block; float: right; width: 16em; padding: 1em; padding-right:0">
  <img style="margin: 0; padding: 0; margin-bottom: 1em; width: 100%" alt="Porträt der Ljot" src="Ljot.jpg"/><br>
  <em style="display: block; text-align: center">Die wilde Ljot</em>
</span>

18. **Kältelabor**. Alles voller Eiszapfen und im Norden ein grosser
    Schneehaufen an der Wand. Eiserne Fleischhaken an der Decke. In
    der Mitte ein dampfender **Sarkophag** aus Eis, darin ein
    schwarzer, steinerner Körper mit einer Krone aus Silber. Eine
    Reihe gläserner Kolben und Destillationsbrücken führen von der
    Brust zu einer großen Auffangschale. Sehr langsam tropft Lavablut
    dort hinein. Unter dem Schnee im Norden schläft ein **Eisteufel**:
    TW 9 RK 2 1W8/1W8/1W12 K9 ML 11 XP 900; kann nur von Magie oder
    magischen Waffen verletzt werden; immun gegen Magie; *Eisbolzen*:
    2W6, Rettungswurf gegen Strahlen für die Hälfte; *Eissturm*: 5W6,
    60 Fuss um den Teufel herum, Rettungswurf gegen Drachenodem für
    die Hälfte. Ohne Stirnreif erwacht **Ljot** in Sekunden zum Leben:
    TW 8 RK 2 1d4/1d4/1d8 K8 BW 12 ML 8 XP 800; *Hitze* verursacht
    1W8/Runde innerhalb von 20 Fuss; immun gegen *Schlaf*,
    *Bezauberung* und andere Zauber, die auf den Geist wirken. ⚠ Bei
    der Kommunikation mit unsterblichen Wesen gilt der Weisheitbonus
    statt der Charismabonus für den Reaktionswurf! („Du wagst es, den
    Stirnreif des Ymir zu berühren?“ – „Der Teufel aus Jötunheim muss
    sterben!“)
   
    > **Stirnreif der Ymir**: Negiert 1W6 Feuerschaden. Kann auf Wusch
    > zur Wasserkondensation, zur Nebelbildung oder sogar zum
    > Schneefall im Umkreis von 5 Fuss verwendet werden. Feuerwesen
    > reagieren allergisch auf den Stirnreif.

19. **Tempel des Ymir**. Statue eines bärtigen Riesen, weisser Marmor
    mit hellblau bemalt. Inschrift: „Gepriesen sei Ymir, Vater und
    Mutter zugleich.“ Dahinter Malereien von einem Eispalast in einem
    wilden Sturm, Riesen, die mit Felsbrocken auf Schiffe werfen,
    Eisberge im Meer, weisse Drachen. Der Statuensockel hat ein
    kleineres Bild, so man im Dunkel Zwerge sieht, die mit Spinnen
    reden, und weiter rechts dann Spinnen, die mit Riesen reden.

20. **Warme Bad der Riesen**. Hier, wo Feuer und Eis sich berühren,
    ist ein heisser Quell entsprungen, der über manch steinerne Stufen
    das Bad füllt. Siehe *Sutr Tempel, Obergeschoss* #3. Das Lachen
    von 20 Wasser leben **Wassernixen**: TW 1 RK 7 1d4 K1 BW 12 ML 6
    XP 100; *Bezauberung*: Rettungswurf gegen Sprüche oder man ist
    eine Woche, einen Tag (Bildung > 12) oder einen Monat (Bildung <
    9) verfallen; *Unterwasseratmung*: eine Person für einen Tag.
    („Die Blutlinge haben den Tempel korrumpiert, um an frisches Blut
    zu kommen.“)

21. **Tempel des Sutr**. Statue eines schwarzen Mannes mit mächtigen
    Hörnern, umwickelt mit einem Umhang aus roter Seide mit einem
    roten Saum (2’000 Gold). Inschrift: „Sutr war der Erste und Sutr
    wird der Letzte sein.“ Dahinter Malereien von einem Flammenmeer,
    in dem Riesen eine grosse Festung errichten. Der Statuensockel hat
    ein kleineres Bild, wo man Sutr und die Eisriesin Katla sieht, wie
    sie Streit haben und überall der Tod regiert, und wie sie wieder
    Frieden schliessen und das Land sich erholt. An der Seitenwand ein
    kleinerer Altar mit einem Bild eines Feuerdämons. Inschrift: „Die
    wilde Ljot.“

22. **Verlassenes Wachzimmer**. Falltüre in der Decke, darüber steht
    ein Bücherregal. Siehe *Sutr Tempel, Untergeschoss* #5.
