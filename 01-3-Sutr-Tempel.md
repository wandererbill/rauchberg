## Sutr Tempel, Untergeschoss

![](01-3-Sutr-Tempel.png)

| 1W6 | Monster                |
|:---:|------------------------|
| 1–3 | 1W6 fliegende Blutegel |
| 4   | Schwarze Rauchschlange |
| 5   | 1W6 Goblinspione       |
| 6   | Svartur der Blutling   |

**Fliegende Blutegel** HD ½ RK 8 1W4 ML 7 EP 100; Blutsauger: haben
sie einmal getroffen, saugen sie sich fest und verursachen ab dann
jede Runde automatisch Schaden. („Das ist mein Blut!“)

**Schwarze Rauchschlange** HD ½ RK 9 – ML 4 EP 100; *Schwäche* für
20 min (reduziert Stärke auf 3) oder Rettungswurf gegen Lähmung;
wächst in Löchern an der Decke; *Überraschung* auf 1–5/6. („Ich kann
mein Schwert nicht heben.“)

**Goblinspione** HD 1-1 RK 6 1W6 ML 7 EP 100; kleine Armbrüste und
lange Dolche bewaffnet. („Zu den Katakomben!“)

**Svartur der Blutling** HD 2 RK 9 1W6 ML 10 EP 200; *Bezauberung* für
1 Tag oder Rettungswurf gegen Sprüche, 2×/Tag. („Folgt mir, wir müssen
zu Leifur!“)

### Raumbeschreibung

1. **Leerer Raum**. Die Türe im Westen ist doppelt verriegelt.

2. **Statue des Bestrafers**. Lava Becken, welches nach Süden langsam
   über eine Kante in die Tiefe fliesst. Wer in die Lava stürzt,
   stirbt. Kein Rettungswurf. Auf der Ostseite steht eine **Statue**
   des schwarzen Sutr mit einer brennenden Peitsche, die sich in der
   heissen Luft etwas bewegt. Wer an den Westrand tritt, wird von der
   Peitsche getroffen und in die Lava gerissen, ausser es gelingt ein
   Rettungswurf gegen Tod. Wenn man der Bestraferstatue in dem Moment,
   wo die Peitsche etwas umfasst hat, ein Schwert zuwirft, lässt sie
   die Peitsche los!
   
    > **Feuerpeitsche**: 1W6 magischer Feuerschaden (kein Schadensbonus
    > im Nahkampf); zieht das Opfer zum Angreifer oder den Angreifer zum
    > Opfer, je nach Grössenunterschied; fügt dem Opfer jede Runde
    > automatisch weiter Schaden zu, ausser der Angreifer löst die
    > Umwicklung; kann man auch als 10 Fuss langes Seil mit Wurfhaken und
    > automatischem Aufzug verwendet werden.
   
3. **Letzte Raum**. Offene Fallgrube in der Mitte. Doppelt verriegelte
   Tür im Westen. Dahinter ein Tunnel. Siehe *Blut Tempel* #4.
   **Fallgrube**: Unten hat es eine Geheimtüre. Siehe *Sutr Tempel,
   Unterwelt* #13.

4. **Urnengräber**. In den Wänden hat es hunderte von Urnen:
   Adalbjorg, Alfdis, Armann, Arna, Aron, … werden sie herausgenommen
   und untersucht, sieht man dass alle sorgfältig angebohrt wurden und
   leer sind.

5. **Archiv**. Einer liest die vielen Bücher, **Svartur**: „Die
   Bewohner Nifelheims“ (Trolle, Eiswölfe); „Die Eisriesen“ (Jötunheim
   ist eine Ebene des ewigen Eises); „Die Herren des Feuers“ (die
   Afarit beherrschen das Feuer, wurden in einem alten Krieg aber
   besiegt); „Die Verehrung des Froschgottes“ (in Myrkheim leben
   Pilzlinge und Zwerge und liegen oft im Streit); „Die 9 Reiche“
   (Asgard für die Götter, Alfheim für die Elfen, Midgard für die
   Menschen, Myrkheim für die Zwerge, Niflheim für die Toten, Vanaheim
   für die Dämonen, Muspelheim für das Feuer, Jötunheim für die
   Frostriesen, und ein unbekanntes Reich); „Die mächtigsten Riesen“
   (Ægir, Hel, Mímir, Narfi, Skaði, Vafþrúðnir, Vörnir, Ymir, Sutr).
   **Versteckte Falltüre** unter einem der Bücherregale. Siehe *Sutr
   Tempel, Unterwelt* #22.

6. **Kerker**. An den Wänden hat es Ketten. Hier liegen Asgerd, Dyri,
   Geir und Idunn, die **Pilger**. („Rettet uns!“) Die **verriegelte
   Türen** führen zu den Gruben und sind von der anderen Seite her
   Geheimtüren.

7. **Vorratskammer**. 20 Speere (60 Gold), 20 Schilde (200 Gold), 10
   Kriegsäxte (70 Gold), 5 Schwerter (50 Gold), 5 Helme (50 Gold), 3
   Fässer mit Stockfisch, 2 Fässer mit Bier (10 Gold). Zwei Kisten:
   nicht verschlossen, leer.

8. **Eckraum**. Falltüre in der Decke, oben mit Teppich verdeckt.

9. **Krypta**. Steinsärge der ehemaligen Sutr Meister des Tempels.
   Namen wie Jokull, Hrefna, Kolli, Svana und Styrr. Keine Untoten.
   Wandmalerei zeigt den Rauchberg, jede Menge Rauch, und einen roten
   Drachen.
