SHELL=/bin/bash
CSS=rauchberg.css

# Create a PDF for every Markdown file
all: Rauchberg.pdf Sutr-Tempel.pdf Ghullande.pdf

# Levels start with a sort number
LEVELS=$(sort $(wildcard [0-9]*.md))
SUTR_LEVELS=$(sort $(wildcard [0-9]*Sutr-Tempel.md))
GHUL_LEVELS=$(sort $(wildcard [0-9]*Ghullande.md))
PICS=$(wildcard *.png)
KARTEN=$(wildcard *.txt)

.PHONY: karten
karten:
	for k in $(KARTEN); do curl https://campaignwiki.org/wiki/Gridmapper/raw/$$(basename $$k .txt) > $$k; done

Lizenz.pdf: Rauchberg.pdf
	pdftk $< cat 3 output $@

upload: Rauchberg.pdf Sutr-Tempel.pdf Ghullande.pdf Lizenz.pdf
	rsync --itemize-changes --archive --compress \
		$^ sibirocobombus:alexschroeder.ch/pdfs/rauchberg/

clean:
	rm -f *.pdf *.epub *.html *.html.tmp Rauchberg.md Sutr-Tempel.md Ghullande.md

test:
	prove t

# --- PDF ---

watch:
	make Rauchberg.pdf
	@echo Regenerating PDFs whenever the .md, *.css or *.png files get saved...
	@inotifywait -q -e close_write -m . | \
	while read -r d e f; do \
	  if [[ "$${f,,}" == *\.md || "$${f,,}" == *\.css || "$${f,,}" == *\.png ]]; then \
	    make Rauchberg.pdf; \
	  fi; \
        done

%.pdf: %.html rauchberg.css
	weasyprint $< $@

%.html: %.html.tmp rauchberg-prefix rauchberg-suffix
	cat rauchberg-prefix $< rauchberg-suffix > $@

%.html.tmp: %.md
	python3 -m markdown \
		--extension=markdown.extensions.tables \
		--extension markdown.extensions.smarty \
		--file=$@ $<

Rauchberg.md: Cover-Rauchberg.md timestamp Vorwort-Rauchberg.md $(LEVELS) $(PICS)
	cat Cover-Rauchberg.md timestamp Vorwort-Rauchberg.md $(LEVELS) > $@

Sutr-Tempel.md: Cover-Sutr-Tempel.md timestamp Vorwort-Sutr-Tempel.md $(SUTR_LEVELS) $(PICS)
	cat Cover-Sutr-Tempel.md timestamp Vorwort-Sutr-Tempel.md $(SUTR_LEVELS) > $@

Ghullande.md: Cover-Ghullande.md timestamp Vorwort-Ghullande.md $(GHUL_LEVELS) $(PICS)
	cat Cover-Ghullande.md timestamp Vorwort-Ghullande.md $(GHUL_LEVELS) > $@

timestamp: $(LEVELS)
	date '+<p class="timestamp">%F</p>' > $@
