## Hels Stein, Erdgeschoß

![](02-1-Hels-Stein.png)

| 1W6 | Zufallsbegegnungen |
|:---:|--------------------|
| 1–3 | 1W6 Dienerinnen    |
| 4   | eine der Hexen     |
| 5   | 1W4 Wachen         |
| 6   | 1W4 Riesenratten   |

| 1W6 | Welche der Hexen?             |
|:---:|-------------------------------|
| 1–2 | Einbla Pfluggesicht           |
| 3–4 | Atallhaug die Schändliche     |
| 5–6 | Aesibra die Gotteslästerliche |

**Dienerin der Hel** TW 1 RK 9 1W4 M1 ML 7 EP 100; 1x Eisblick 1W4
gegen einen in 20 Fuss, Rettungswurf oder Lähmung für 1 Phase („…“)

**Einbla Pfluggesicht** TW 4 TP 21 RK 4 1W3/1W3 M4 ML 9 EP 400; Zauber
jeder einmal pro Tag: Magisches Geschoß, Person bezaubern, Schild,
Hexenaug’, Unsichtbarkeit; Verwandlung in Katze; („Ich will mit dir
spielen, Mäuschen!“)

**Aesibra die Gotteslästerliche** TW 5 TP 20 RK 4 1W3/1W3 M4 ML 9
EP 500; Zauber jeder einmal pro Tag: Magisches Geschoß, Person
bezaubern, Schild, Hexenaug’, Unsichtbarkeit, Personen festhalten;
Verwandlung in Schlange; („Komm näher, Kaninchen, komm näher!“)

**Atallhaug die Schändliche** TW 6 TP 29 RK 4 1W3/1W3 M4 ML 9 EP 600;
Zauber jeder einmal pro Tag: Magisches Geschoß, Person bezaubern,
Schild, Hexenaug’, Unsichtbarkeit, Einflüsterung, Personen festhalten;
Verwandlung in Rabe; („Deine Augen sehen schmackhaft aus!“)

**Zauber der Hexen**: Hexenaug’, 2. Zirkel, einer in 90 Fuß,
Rettungswurf oder Verflucht bis zum nächsten Gebet das mindestens eine
Phase dauert, -2 auf alle Würfe, funktioniert auch heimlich.

**Äbtissin Aesifasta** TW 9 TP 45 RK 9 1W4+2 ML 10 EP 900; jederzeit
*Eisblick*; 3x Personen festhalten (bis zu 4 Personen in 30 Fuß, RW oder
Gelähmt), 3x Mit Toten sprechen (beantwortet 3 Fragen); 1x Schutz vor
Bösem (10 Fuß Radius, außerweltliche Kreaturen können nicht direkt
berühren), 1x Schwere Wunden heilen (3W6+3 TP), 1x Böses bannen
(Außerweltliche Kreaturen werden in die Astrale See geschleudert),
Finger des Todes (ein Wesen in 30 Fuß, RW oder erschlagen); („Die
Gnade Hels bleibt euch verwehrt, solange ihr eurem närrischen Tun
nicht abschwört.“)

> **Dolch des Todes**: Schaden +2, immun gegen Lebenskraftentzug,
> stattdessen für 1W6 Stunden herumirren in der Unterwelt bis Rückweg
> gefunden.

### Allgemeines

Die Dienerinnen der Hel sind ein Nonnenorden mit Schweigegelübde
gegenüber Außenstehenden - und sie unterhalten sich nach Möglichkeit
auch nicht vor Fremden. Sie kümmern sich um die Toten, die weder in
der Schlacht umkamen noch ertrunken sind. Die lange und knochige
Äbtissin Aesifasta die Kundige kann einen solchen SC wieder zum Leben
erwecken, wenn mindestens 10 NSC pro Stufe persönlich vorstellig
werden, und sie darum bitten – aber die Zweigesichtige Göttin verlangt
noch einen besonderen Dienst von denjenigen, die aus ihrem Reich
entlassen werden. Drei der Nonnen sind Hexen, die sich ganz und gar
dem Dienst an der dunklen Seite Hel-Hekates verschrieben haben, ohne
das die anderen Nonnen davon wissen.

Bei Hels Stein handelt es sich um einen großen, 90m hohen und am Fuß
60m durchmessenden Monolithen. Die Seite, die dem Rauchberg zugewandt
ist, ist schwarz und verbrannt, während die vom Vulkan abgewandte
Seite aus hellem, glitzerndem Fels besteht. Das Gesicht einer Frau ist
in den unteren Teil der Südwand gehauen, eine Seite dunkel und streng,
die andere hell und gutmütig. Offenstehender Mund am Boden und leere
Augenhöhlen etwa 9m über dem Boden.

### Raumbeschreibung

1. Ein Schild mit der Aufschrift „Nur nahe Verwandte oder Freunde
   haben Eintritt mit Verstorbenen“ mit dem handschriftlichen Zusatz
   „Und keine Betrunkenen!“

2. Tagsüber eine der **Dienerinnen**. Wenn kein Toter dabei ist,
   versucht sie Eindringlinge herauszuscheuchen. Nachts zwei der
   **Wachen** von #3.

 3. 6 **Wachen** ohne Zungen, Raum mit einfachen Strohbetten.
	Reagieren auf Ärger am Eingang. TW 1-1 RK 7 1W6 ML 8 EP 100
	(„Hm“). Einer von ihnen trägt eine Goldkette (25 Gold). Oberwache:
	TW 2 RK 4 1W8 ML 8 EP 200 („Hrmm“).

 4. Große Halle, Steinplatte zur Aufbahrung, Lift für 2 Personen nach
    oben (siehe 2 - 15), Tagsüber 1W6-1 **Nonnen** anwesend - 1W6+4
    und die **Äbtissin** sowie 1W6 **Familienmitglieder**, wenn eine
    Einbalsamierung vorgenommen wird. Thron für die Äbtissin aus
    glänzend polierten Drachenknochen (500 Pfund, 5’000 Gold wert).
    Drachenklaue zeigt auf kleinen Knopf für Geheimtür.
    Einbalsamierwerkzeug (500 Gold).

 5. Speisesaal, Tische und Bänke, gepolsteter Sessel an einem der
	Tischenden, Abbildungen der Gutmütigen Hel, Speisezeiten sind
	Sonnenaufgang und Sonnenuntergang

 6. Küche, Ofen befeuert von Lava, Regale voller Geschirr und
	Nahrungsmittel, Abbildungen der Strengen Hel. Die Köchin (Hexe)
	**Aesibra** befindet sich immer hier, mit vor Sonnenaufgang und
	Sonnenuntergang 1W4-1 **Nonnen** als Küchengehilfinnen. 1-2 in W6
	in der Nacht am Tränkebrauen.

 7. Andachtskapelle, Schmucklos, nur 10 Gebetsbänke mit jeweils einem
	Gebetsbuch (10 GM), Tagsüber 1W6-1 **Dienerinnen** anwesend.

 8. Einfacher Abort. Bei 1 in W6 flieht **Riesenratte** hinein, wenn
	der Raum betreten wird.

 9. Treppe in die Krypten, Flankiert von Abildungen der dualen Hel.

10. Schlafraum für 6 Nonnen, einfache Strohbetten mit einer kleinen
	Truhe. Tagsüber 1W4-1 anwesend, nachts 6. In einer Truhe
	verstecktes Goldmedallion mit Haarlocke von Geliebtem (125 GM).

11. Aufenthaltsraum, einfacher Tisch 6 Stühle, Tagsüber 1W8-1
	**Dienerinnen** anwesend. Regal vor der Südwand mit religiöser
	Literatur (insgesamt 500 GM und 200 Pfund Gewicht) und Würfel mit
	Becher, kann beiseite geschwungen werden, Geheimtür nach Raum 4.

12. Schlafraum, 6 einfache Strohbetten mit einer kleinen Truhe.
	Tagsüber 1W4-1 **Nonnen* anwesend, nachts 5.

13. Geheimgang der Hexen, Geheimtüre: kleine Klappen am Boden, 1*1
	Fuß. Regal mit 4 Tränken: Heiltrank (15 TP), Gift, Verwirrung,
	Gasförmigkeit, Beutelchen voller Giftpflanzen und -pilzen.
	Abgenagte Kinderknochen.

14. Grobe Zeichnungen und Schriftzeichen über schlecht versteckter
	Falltür mit Leiter. „Wenn das Licht der Nacht am Himmel schwindet,
	tief herab der Weg sich windet!“.
