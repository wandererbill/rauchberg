## Vorwort

Ende April 2022 haben sich ein paar Leute gefragt, ob es denn einen
deutschen Megadungeon für klassisches D&D gibt (Basic und Expert D&D,
Labyrinth Lord, Old School Essentials, [Hellebarden &
Helme](https://alexschroeder.ch/wiki/Hellebarden_%26_Helme), und so
weiter). Gibt es nicht, hiess es, und eine Übersetzung von einem
riesigen Dungeon würde sich auch niemals lohnen. Nun, dann bleibt uns
ja nur selber machen…

<p class="fit"><img alt="Eine handgezeichnete Übersichtskarte von
einem Megadungeon, oben ein Vulkan, am Hang ein Feuertempel für Sutr,
dann eine Goblinstadt, eine verlassene Zwergenbinge, ein
Spinnenreiche, eine Trollfeste, ein Lavasee mit Drache, Zugang zu
Muspelheim, Styx, Charon, Niđhögr, Yggdrasilwurzel…"
src="Rauchberg-Übersicht.png" /></p>

Angefangen haben wir ursprünglich mit einer schnellen Skizze. Damals
wussten wir natürlich noch nichts from Prinz der Asche, vom Trollkönig,
und all den anderen. Das war alles noch in weiter Ferne.

Um was geht es hier also? Um einen “Dungeon” – eine Abenteuerumgebung,
wo die Aktionen der Spieler durch die Karte limitiert ist. Welche Türe
öffnen wir? Welchen Gang gehen wir entlang? Ganz wichtig ist an dieser
Stelle ein Hinweis auf die Reaktionstabelle. Verwendet diese! Es gibt
in diesem Dungeon keine Monster, welche die Spieler ohne vorherige
Provokation oder vorherigen schlechten Ausgang des Reaktionswurfes
angreifen. Es sind im Gegenteil die Spieler, welche mit ihren
Personnagen immer tiefer in die Unterwelt steigen, sich ihre Feinde
selber wählen, sich in fremde Händel einmischen, habgierig und
hochmutig werden, Fehler machen, und am Schluss in Kämpfe verwickelt
werden. In einer erfolgreichen Expedition in die Unterwelt werden
Schätze geborgen ohne zu kämpfen, ohne zu würfeln, denn wenn man
würfeln muss, ist es eigentlich schon zu spät.

In diesem Dungeon gibt es keine “fairen” Kämpfe. Es liegt an den
Spielern, nur dort zu kämpfen, wo sie einen überdeutlichen taktischen
Vorteil haben. Die spannende Frage ist nicht, wie der Gegner besiegt
wird, sondern gegen welchen Gegner überhaupt gekämpft werden soll.

Die Spielleitung muss sich bei Fallen und Überraschungen ebenfalls
zurückhalten. Wenn eine Personnage um die Ecke geht, von einem Drachen
überrascht wird und stirbt oder einen Haufen von Lumpen in der Ecke
untersucht, von einer Schlange überrascht und gebissen wird und
stirbt, dann ist das nicht cool. Warum hat man den Drachen nicht atmen
gehört? Warum hat die Schlange nicht vorher gedroht? Diese Zeichen zu
sehen, zu ignorieren, und dann den Preis dafür zu zahlen, dass ist das
quintessentielle Dungeonerlebnis.

Tipps zur Charaktererschaffung gibt es keine ausser einem Hinweis auf
Fremdsprachen: Durch den Handel mit der Goblinstadt könnte man deren
Sprache sprechen. Die Hyänenmenschen sind gewalttätige und brutale
Typen, welche behaupteterweise auch Menschenfleisch nicht verschmähen.
Ihre Sprache könnte man auch gelernt haben. Und dann natürlich die
Sprache des Feuers selber. Ein Muss für alle Pyromanten.

### Auf ins Abenteuer!

Und hier kommt dann die Danksagung und ein Wunsch auf frohes Spiel.

## Lizenz

Die Karten und Raumbeschreibungen stehen unter der Creative Commons
„Namensnennung - Weitergabe unter gleichen Bedingungen 4.0
International“ (CC BY-SA 4.0) Lizenz. Eine Kopie der Lizenz gibt es
hier:
[creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/deed.de).
Was folgt ist eine einfache Zusammenfassung der (und kein Ersatz für)
die Lizenz.

Sie dürfen:

- **Teilen** – das Material in jedwedem Format oder Medium
  vervielfältigen und weiterverbreiten

- **Bearbeiten** – das Material remixen, verändern und darauf aufbauen

Der Lizenzgeber kann diese Rechte nicht widerrufen, solange Sie sich
an die Lizenzbedingungen halten.

- **Namensnennung** – Sie müssen angemessene Urheber- und
  Rechteangaben machen, einen Link zur Lizenz beifügen und angeben, ob
  Änderungen vorgenommen wurden. Diese Angaben dürfen in jeder
  angemessenen Art und Weise gemacht werden, allerdings nicht so, dass
  der Eindruck entsteht, der Lizenzgeber unterstütze gerade Sie oder
  Ihre Nutzung besonders.

- **Weitergabe unter gleichen Bedingungen** – Wenn Sie das Material
  remixen, verändern oder anderweitig direkt darauf aufbauen, dürfen
  Sie Ihre Beiträge nur unter derselben Lizenz wie das Original
  verbreiten.

- **Keine weiteren Einschränkungen** – Sie dürfen keine zusätzlichen
  Klauseln oder technische Verfahren einsetzen, die anderen rechtlich
  irgendetwas untersagen, was die Lizenz erlaubt.

Die Bilder stehen unter der Creative Commons „Namensnennung - Keine
Bearbeitungen 4.0 International“ (CC BY-ND 4.0) Lizenz. Eine Kopie der
Lizenz gibt es hier:
[creativecommons.org/licenses/by-nd/4.0/](https://creativecommons.org/licenses/by-nd/4.0/deed.de).
Was folgt ist eine einfache Zusammenfassung der (und kein Ersatz für)
die Lizenz.

Sie dürfen:

- **Teilen** – das Material in jedwedem Format oder Medium
  vervielfältigen und weiterverbreiten

Der Lizenzgeber kann diese Rechte nicht widerrufen, solange Sie sich
an die Lizenzbedingungen halten.

- **Namensnennung** – Sie müssen angemessene Urheber- und
  Rechteangaben machen, einen Link zur Lizenz beifügen und angeben, ob
  Änderungen vorgenommen wurden. Diese Angaben dürfen in jeder
  angemessenen Art und Weise gemacht werden, allerdings nicht so, dass
  der Eindruck entsteht, der Lizenzgeber unterstütze gerade Sie oder
  Ihre Nutzung besonders.

- **Keine Bearbeitung** – Wenn Sie das Material remixen, verändern
  oder darauf anderweitig direkt aufbauen, dürfen Sie die bearbeitete
  Fassung des Materials nicht verbreiten.

- **Keine weiteren Einschränkungen** – Sie dürfen keine zusätzlichen
  Klauseln oder technische Verfahren einsetzen, die anderen rechtlich
  irgendetwas untersagen, was die Lizenz erlaubt.

Hinweise:

Sie müssen sich nicht an diese Lizenz halten hinsichtlich solcher
Teile des Materials, die gemeinfrei sind, oder soweit Ihre
Nutzungshandlungen durch Ausnahmen und Schranken des Urheberrechts
gedeckt sind.

Es werden keine Garantien gegeben und auch keine Gewähr geleistet. Die
Lizenz verschafft Ihnen möglicherweise nicht alle Erlaubnisse, die Sie
für die jeweilige Nutzung brauchen. Es können beispielsweise andere
Rechte wie Persönlichkeits- und Datenschutzrechte zu beachten sein,
die Ihre Nutzung des Materials entsprechend beschränken.

### Autoren von Karten und Raumbeschreibungen

- Alex Schroeder, https://alexschroeder.ch/
- Rorschachhamster, https://rorschachhamster.wordpress.com/
- Sören Kohlmeyer, https://paradoxcafe.de/

### Autoren von Bildern

- Nioi-Sumire, https://rollenspiel.social/@Sumire
