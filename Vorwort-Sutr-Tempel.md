## Einleitung

Der Sutr Tempel ist ein alter Tempel für den Feuerdämon Sutr. Der
Tempel gehört zum
[Rauchberg](https://campaignwiki.org/wiki/Rauchberg/) Megadungeon. Für
sich alleine genommen, muss die Spielleitung entscheiden, was mit den
Ausgängen zum Blut Tempel oder zur Goblinstadt geschehen soll.

In diesem Dungeon gibt es keine “fairen” Kämpfe. Es liegt an den
Spielern, nur dort zu kämpfen, wo sie einen überdeutlichen taktischen
Vorteil haben. Die spannende Frage ist nicht, wie der Gegner besiegt
wird, sondern gegen welchen Gegner überhaupt gekämpft werden soll.

Verwendet die Reaktionstabelle. Im Kontakt mit Unsterblichen verwende
ich gerne die Weisheit statt das Auftreten (Charisma) für den Bonus
auf den Wurf.

Spielberichte gerne per E-Mail an alex@alexschroeder.ch.

### Lizenz

Dieses Werk ist unter der Creative Commons „Namensnennung -
Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0
International“ (CC BY-NC-SA 4.0) Lizenz. Eine Kopie der Lizenz gibt es
hier:
[creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de).
Was folgt ist eine einfache Zusammenfassung der (und kein Ersatz für)
die Lizenz.

Sie dürfen:

- **Teilen** – das Material in jedwedem Format oder Medium
  vervielfältigen und weiterverbreiten

- **Bearbeiten** – das Material remixen, verändern und darauf aufbauen

Der Lizenzgeber kann diese Rechte nicht widerrufen, solange Sie sich
an die Lizenzbedingungen halten.

- **Namensnennung** – Sie müssen angemessene Urheber- und
  Rechteangaben machen, einen Link zur Lizenz beifügen und angeben, ob
  Änderungen vorgenommen wurden. Diese Angaben dürfen in jeder
  angemessenen Art und Weise gemacht werden, allerdings nicht so, dass
  der Eindruck entsteht, der Lizenzgeber unterstütze gerade Sie oder
  Ihre Nutzung besonders.

- **Nicht kommerziell** – Sie dürfen das Material nicht für
  kommerzielle Zwecke nutzen.

- **Weitergabe unter gleichen Bedingungen** – Wenn Sie das Material
  remixen, verändern oder anderweitig direkt darauf aufbauen, dürfen
  Sie Ihre Beiträge nur unter derselben Lizenz wie das Original
  verbreiten.

- **Keine weiteren Einschränkungen** – Sie dürfen keine zusätzlichen
  Klauseln oder technische Verfahren einsetzen, die anderen rechtlich
  irgendetwas untersagen, was die Lizenz erlaubt.

Hinweise:

Sie müssen sich nicht an diese Lizenz halten hinsichtlich solcher
Teile des Materials, die gemeinfrei sind, oder soweit Ihre
Nutzungshandlungen durch Ausnahmen und Schranken des Urheberrechts
gedeckt sind.

Es werden keine Garantien gegeben und auch keine Gewähr geleistet. Die
Lizenz verschafft Ihnen möglicherweise nicht alle Erlaubnisse, die Sie
für die jeweilige Nutzung brauchen. Es können beispielsweise andere
Rechte wie Persönlichkeits- und Datenschutzrechte zu beachten sein,
die Ihre Nutzung des Materials entsprechend beschränken.

### Autoren

- Alex Schroeder, https://alexschroeder.ch/
- Rorschachhamster, https://rorschachhamster.wordpress.com/
- Sören Kohlmeyer, https://paradoxcafe.de/
- Nioi-Sumire, https://rollenspiel.social/@Sumire
