## Sutr Tempel, Erdgeschoss

![](01-1-Sutr-Tempel.png)

| 1W6 | Monster             |
|:---:|---------------------|
| 1–3 | 1W6 Akolyten        |
| 4   | Graue Rauchschlange |
| 5   | 1W6 Goblinspione    |
| 6   | Refr der Blutling   |

**Akolyten** HD 1 RK 9 1W6 ML 6 EP 100; *brennende Hände* 1W6 gegen
alle innerhalb von 10 Fuss. („Wartet nur, der Prinz der Asche wird euch
noch holen!“)

**Graue Rauchschlange** HD 1 RK 9 – ML 4 EP 100; *Lähmung* für 20 min
oder Rettungswurf; kriecht langsam in den dunklen Ecken herum
*Überraschung* auf 1–4/6. („Ich… kriege… keine… Luft!“)

**Goblinspione** HD 1-1 RK 6 1W6 ML 7 EP 100; kleine Armbrüste und
lange Dolche bewaffnet. („Bitte tut uns nichts, wir sind durch die
Krypta gekommen…“)

**Refr der Blutling** HD 2 RK 9 1W6 ML 10 EP 200; *Bezauberung* für 1
Tag oder Rettungswurf gegen Sprüche, 2×/Tag. („Folgt mir, wir müssen zu
Leifur ins Obergeschoss!“)

### Allgemeines

Der Tempel ist in die Vulkanflanke hineingebaut. Von aussen sieht man
im Obergeschoss sechs Fenster, von denen aus man den Zugang zum Tempel
kontrollieren kann und unten breite Treppen, die zu einer Doppeltüre
führen. Die Pilger Asgerd, Dyri, Geir und Idunn werden vermisst…

### Raumbeschreibung

 1. **Grosse Halle**. Säulen, Statue eines schwarzen Mannes mit
	mächtigen Hörnern, in der einen Hand ein Schwert, in der anderen
	eine Peitsche. 3 **Akolyten**, die behaupten, der Tempel sei
	geschlossen.

 2. **Kleines Wachtzimmer**. Schreibtisch. Bett. Teppich (50 Gold).
	Chance von 1–5/6, dass **Refr der Blutling** hier ist. **Teppich**:
	eine versteckte Falltüre. **Schreibtisch**: Das Buch „Schmiedkunst
	der Söhne Ymirs“ (ein Frostriese in Jötunheim), welches unter
	anderem das Schwert *Rauhreif* beschreibt, und wie Loki es Bor
	gestohlen habe und aus dem Leder vom Riesenstier Graðungr einen
	Gürtel der Riesenstärke genäht hat.

 3. **Kleines Wachtzimmer**. Teppich (50 Gold). Der bezauberte
    Goblinspion **Zafir** sitzt hier und wartet auf seinen besten
    Freund, Refr. Wasserkrug. Stockfisch. Brot.

 4. **Grube**. Eine offene Grube, 10 Fuss tief, 1W6 Schaden wer
	hinfällt. Unten hat es eine von aussen verriegelte Türe. Nach oben
	geht der Schacht weiter und führt ins Obergeschoss.

 5. **Falle**. Nach oben führt ein kurzer Schacht und endet an einer
	Holzdecke 10 Fuss über dem Boden, die irgendwie seltsam kompliziert
	gestützt ist. Ein Dieb erkennt, dass es sich um eine Falltüre
	handelt. Selbst steht man hier auch über einer **versteckten
	Fallgrube**, 10 Fuss tief, 1W6 Schaden wer hinfällt. Unten hat es
	eine von aussen verriegelte Türe.

 6. **Innere Tempel**. Vorne steht die Statue eines schwarzen Mannes
	mit mächtigen Hörnern, in der einen Hand ein Schwert, in der
	anderen eine Peitsche. Daneben die Trümmer des gleichen Mannes, in
	der einen Hand eine Laterne, in der anderen Feuerholz. Zerrissene
	Wandvorhänge mit Meditationen zum Thema Feuer, im Guten wie im
	Schlechten.

 7. **Leerer Raum**. Schwefelgeruch. Man hört die Lava im Schacht
    brodeln.

 8. **Linke Kammer**. Vorratfässer mit Stockfish (50 Gold). Alte
    Robbenfelle (50 Gold).

 9. **Rechte Kammer**. 2 Kisten. Wolldecken. Brettspiel mit Figuren
    aus Walrosselfenbein (30 Gold).

10. **Lava Schacht**. Unten hat es Lava, aber auch einen Sims im Osten
	und Westen. Siehe *Sutr Tempel, Unterwelt* #2. Wer in die Lava
	stürzt, stirbt. Kein Rettungswurf. Nach oben dasselbe. Siehe *Sutr
	Tempel, Obergeschoss* #9.
