## Sutr Tempel, Obergeschoss

![](01-2-Sutr-Tempel.png)

| 1W6 | Monster             |
|:---:|---------------------|
| 1–3 | 1W6 Akolyten        |
| 4   | Gelbe Rauchschlange |
| 5   | 1W6 Goblinspione    |
| 6   | Meister Leifur      |

**Akolyten** HD 1 RK 9 1W6 ML 6 EP 100; *brennende Hände* 1W6 gegen
alle innerhalb von 10 Fuss. („Meister Leifur hat es befohlen und so
soll es geschehen!“)

**Gelbe Rauchschlange** HD 1-1 RK 9 – ML 4 EP 100; *Atemnot* für
20 min (nur noch langsames Gehen möglich) oder Rettungswurf; kriecht
langsam an der Decke herum; *Überraschung* auf 1–4/6. („Keine… Luft…“)

**Goblinspione** HD 1-1 RK 6 1W6 ML 7 EP 100; kleine Armbrüste und
lange Dolche bewaffnet. („Das Sonnenlicht ist so verdammt hell, wie
halten die das aus?“)

**Meister Leifur** HD 5 RK 9 1W6 ML 6 EP 500; 1× *Feuerball*, 2×
*Feuerklinge*, 3× *Feuerpfeil*. („Gnade! Ich bringe euch die
Geheimnisse der Feuermagie bei!“)

> **Zauberbuch des Meister Leifur**: *Feuerball* (3) 5W6 mit Radius
> 20 Fuss, Rettungswurf gegen Sprüche für halben Schaden;
> *Feuerklinge* (2) macht aus dem Dolch einen Flammendolch für 2W6
> Schaden; *Feuerfalle* (2) für 4W6, Rettungswurf gegen Sprüch für
> halben Schaden; *Feuerresistenz* (1) 1W6/Runde für 1h; *Feuerpfeil*
> (1) schiesst 3 Pfeile zu je 1W8, trifft immer.

### Raumbeschreibung

 1. **Galerie**. Die beiden Galerien erlauben einen Blick in die
	Halle. Hier ist man auf Augenhöhe mit Sutr. Eine Brücke verbindet
	die beiden Seiten.

 2. **Wachraum**. Kleiner Tisch, zwei Stühle. Schrank mit 50 Pfeilen.

 3. **Wasserbecken**. Trinkwasserbrunnen. Das Wasser fliesst in ein
	Rohr, wo man in der Tiefe plätschern und Frauen lachen hören kann.
	Siehe *Sutr Tempel, Unterwelt* #20. Die Inschrift auf Zwergisch:
	„Ein warmes Bad ist der Segen von Sutr und Ymir – Gespendet von
	der Familie Messingtor“

 4. **Schiessfenster**. Kleine Zelle mit Fenster, wo man den Zugang zum
	Tempel schützen kann. Leer.

 5. **Pausenraum**. Tisch. Stühle. 3 **Akolyten** spielen ein
	Würfelspiel.

 6. **Schlafräume**. Tagsüber schlafen hier die 3 **Akolyten**, welche
	Nachtwächterdienst geleistet haben.

 7. **Büro von Meister Leifur**. Schreibtisch. Stuhl. Bett. Kiste.
	Teppich (250 Gold). Chance von 1–5/6, dass **Meister Leifur** hier
	ist. **Schreibtisch**: Tintenfass mit Saphir (100 Gold). Buch
	„Queste derer von Messingtor auf Suche nach der alten Zwergenbinge
	von Grimmlicht“, in welchem erzählt wird, wie Kolskeggr, Sohn des
	Kolmundurs, versucht hat, die Binge zu finden. Am Schluss hat
	Kolskeggr den Zwergenturm „an der dritten Brücke“ erobert und neu
	befestigt. **Kiste**: Wäsche, *Dolch* +2 mit der Namensrune des
	Kolmundur Messingtor. An der Südwand hat es ein ein
	russgeschwärztes Relief von einem Kopf mit Hörnern. Wird der Kopf
	mit Feuer geheizt, entriegelt sich eine **Geheimtüre**.

 8. **Privater Sutr Schrein**. Altar. Statue. Auf der Stirn des
	schwarzen Sutr ein blutroter Rubin (750 Gold). Wird der Rubin
	entfernt, hört man ein Seufzen und Sutr sagt: „Ich mag all das
	Blut nicht mehr sehen!“
   
 9. **Feuergrube**. Tiefe Grube. Unten hat es Lava, aber auch einen
	Sims im Osten und Westen. Siehe *Sutr Tempel, Unterwelt* #2. Wer
	in die Lava stürzt, stirbt. Kein Rettungswurf.

10. Im Gang hat es eine **offene Falltüre** (2W6 Fallschaden bis ins
    Untergeschoss, da die Falltüre im Erdgeschoss ebenfalls offen
    ist), eine **geschlossene Falltüre** (1–2/6, dass sie sich öffnet,
    1W6 Fallschaden bis ins Erdgeschoss, dort nochmals 1–2/6, dass
    diese sich ebenfalls öffnet, weitere 1W6 Fallschaden bis ins
    Untergeschoss). Die **falsche Türe** am Ende besteht aus ein paar
    Brettern, die an die Wand genagelt sind.
