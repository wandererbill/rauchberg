## Ghullande, Brücke

![](04-1-Ghullande.png)

| 1W6 | Monster                    |
|:---:|----------------------------|
| 1   | 2W6 Skelette               |
| 2   | 1W6 Zombies                |
| 3   | 1W6 Goblinspione           |
| 4   | 1W6 Hyänenmenschen         |
| 5   | 1W6 Ghule                  |
| 6   | falls gelb: 1 Knochengolem |
|     | ansonsten: Edelghul Svana  |

**Skelette** TW 1 RK 7 1W6 K1 BW 6 ML 7 EP 100; Speer; immun gegen
*Schlaf*, *Bezauberung* und andere Zauber, die auf den Geist wirken.
(„Wir sind die Wächter!“)

**Zombies** TW 2 RK 8 1W8 K1 BW 12 ML 12 EP 200; Macheten; immun gegen
*Schlaf*, *Bezauberung* und andere Zauber, die auf den Geist wirken.
(„Wir sind die Träger!“)

**Goblinspione** TW 1-1 RK 6 1W6 K1 BW 6 ML 7 EP 100; versteckte
Langmesser. („Wir suchen den Knochenmagier Etzel.“)

**Hyänenmenschen** TW 2+1 RK 5 1W10 K2 BW 9 ML 8 EP 200; Kette,
Zweihänder, Dolch. („Gagarr wurde gefangen genommen.“)

**Ghule** TW 2 RK 6 1d4/1d4/1d4 K2 BW 9 ML 9 XP 200; *Lähmung* für 1h
oder Rettungswurf gegen Lähmung; **Aura der Angst** gegen eine Person
innerhalb von 30 Fuss: Flucht für 2 Runden oder Rettungswurf; gegen
Sprüche; **Hyänengestalt** nach belieben. („Frischfleisch! Seid ihr
schon reserviert?“)

**Knochengolem** TW 8 RK 2 1d6/1d6/1d6/1d6 K16 BW 12 ML 12 XP 800;
*immun gegen Pfeile*; *immun gegen Gift*; *immun gegen Sprüche*; wird
ein Golem zerlegt, findet man in der Lebensmatrix 2W6 Edelsteine 1W20:
1–3 → 10, 4–6 → 20, 7–9 → 50, 10–12 → 75, 13–15 → 100, 16–17 → 250,
18–19 → 750, 20 → 1000 Gold.

**Edelghul Svana** TW 5 RK 6 1d4/1d4/1d4 K2 BW 9 ML 9 XP 500;
*Lähmung* für 1h oder Rettungswurf gegen Lähmung; **Aura der Angst**
gegen eine Person innerhalb von 30 Fuss: Flucht für 2 Runden oder
Rettungswurf; gegen Sprüche; **Hyänengestalt** nach belieben.
(„Seid ihr neu hier?“)

### Raumbeschreibung

 1. **Erste Brücke**. Breite Brücke durch den rot beleuchteten
    Vulkankrater. Links und rechts Befestigungsanlagen mit
    Schiessscharten. **Ghul Hrefna** mit 6 **Skeletten** wacht hier.
    („Wer in Frieden kommt, ist willkommen.“) Vor allem stellen sie
    sicher, dass niemand nach Osten flieht.

 2. **Brückenende**. Zum feuerroten Schlund hin offen. 4 **Ghule**.
    („Wir gehören zum Haus Hrossabein.“)

 3. **Empfangshalle**. 4 **Ghule** und **Edelghul Krókóttr** auf
    Brokatstühlen, rauchen Opium. Um sie herum 4 verängstigte
    Menschen. („Wer hat euch eingeladen?“)

 4. **Eis Kreuzung**. Kaltes Licht von Norden. Dort sieht man die
    Statue von Hel und einen grossen Wolf (→ #6).

 5. **Knochenlager**. Der Raum enthält 20 **Skelette** des Hauses
    Hrossabein, die auf Befehle warten.

 6. **Hel Statue**. Zu ihren Füssen der **Eiswolf Drífa** TW 5 RK 4
    1W6 ML 9 EP 500; jeder Angriff hat eine 1–2/6 Chance, *Eisatem*
    für 5W6, Rettungswurf gegen Drachenodem für die Hälfte; immun
    gegen Kälte; sieht unsichtbares. („Wer Hel nicht dient, hat hier
    nichts verloren!“)

 7. **Andachtsraum des Drachens**. Ätzender Geruch. Die Skulptur von
    Baumwurzeln, an denen ein Drachen nagt, umringt von Skeletten.
    **Inschrift**: „Oh Niđhögr, Herr des Gifts, antworte mir!“ Wer die
    Türe schliesst, kann mit Niđhögr in Verbindung treten. Chance von
    1–2/6, dass der Drache nach einer Antwort „ausatmet“ und den Raum
    mit Giftgas füllt. Tod in 1W6×10min oder Rettungswurf gegen Gift.

 8. **Eishöhle**. 120 tiefgefrorene Leichen liegen hier, von Schnee
    und Eis überzogen.

 9. **Warteraum**. Steinbänke zum Sitzen. An der Türe im Westen das
    Bild einer Frau mit dreiköpfigem Hund.

10. **Offene Grabkammer**. Unzählige Gräber in den Wänden, die Leichen
    vertrocknet.

11. **Waffenkammer**. Regale mit 400 Pfeilen und 50 Speeren.

12. **Erste Schützenkammer Nord**. Schiessscharten mit Blick auf die
    Brücke (→ #1). 4 **Skelette** mit Bögen.

13. **Zweite Schützenkammer Nord**. Schiessscharten mit Blick auf das
    Brückenende (→ #2). 4 **Skelette** mit Bögen.

14. **Statue des Garm**. Ein mehrköpfiger Hund. **Inschrift**: „Garm
    bewacht Hels Reich.“

15. **…**.

16. **Hrossabein Halle**.

17. **…**.

18. **…**.

19. **…**.

20. **…**.

21. **…**.

22. **Urds Statue**. Eine Frau an einem Quell, umringt von Wurzelwerk.
    **Inschrift**: „Wer die Fäden spinnt, “.

23. **Zimmer des Bruđamađr**. Hier lebt der **Edelghul Bruđamađr**,
beschützt von 3 **Strohgolems** TW 1 RK 9 1d6 K2 BW 12 ML 12 XP 100;
*immun gegen Pfeile*; *immun gegen Gift*; *immun gegen Sprüche*; wird
ein Golem zerlegt, findet man in der Lebensmatrix einen Edelstein im
Wert von 1W6×10 Gold. („Wenn ihr nicht verschwindet, rufe ich den
Knochengolem!“)

24. **…**.

25. **…**.

26. **…**.

27. **…**.

28. **…**.

29. **…**.

30. **…**.

31. **Festsaal**.
<span style="text-align: center; display: block">
  <img style="float: none; width: 80%; margin: auto; padding: 0"
       alt="Die Ghuldame serviert dem König den Kopf des Ivarr" src="Ghule.jpg"/><br>
  <em style="display: block; text-align: center">Die Menschenfresser</em>
</span>

32. **…**.

33. **…**.

34. **…**.

35. **…**.

36. **…**.

37. **…**.

