## Ghullande, die Gräberstadt

![](04-3-Ghullande.png)

| 1W6 | Monster                    |
|:---:|----------------------------|
| 1   | 2W6 Skelette               |
| 2   | 1W6 Zombies                |
| 3   | ?                          |
| 4   | ?                          |
| 5   | 1W6 Ghule                  |
| 6   | falls gelb: 1 Knochengolem |
|     | ansonsten: ?               |

**Skelette** TW 1 RK 7 1W6 K1 BW 6 ML 7 EP 100; immun gegen *Schlaf*,
*Bezauberung* und andere Zauber, die auf den Geist wirken.

**Ghule** TW 2 RK 6 1d4/1d4/1d4 K2 BW 9 ML 9 XP 200; *Lähmung* für 1h
oder Rettungswurf gegen Lähmung; **Aura der Angst** gegen eine Person
innerhalb von 30 Fuss: Flucht für 2 Runden oder Rettungswurf; gegen
Sprüche; **Hyänengestalt** nach belieben. („Frischfleisch! Seid ihr
schon reserviert?“)

**Knochengolem** TW 8 RK 2 1d6/1d6/1d6/1d6 K16 BW 12 ML 12 XP 800;
*immun gegen Pfeile*; *immun gegen Gift*; *immun gegen Sprüche*; wird
ein Golem zerlegt, findet man in der Lebensmatrix 2W6 Edelsteine 1W20:
1–3 → 10, 4–6 → 20, 7–9 → 50, 10–12 → 75, 13–15 → 100, 16–17 → 250,
18–19 → 750, 20 → 1000 Gold.

### Raumbeschreibung

 1. **…**.

 2. **…**.

 3. **…**.

 4. **…**.

 5. **…**.

 6. **…**.

 7. **…**.

 8. **…**.

 9. **…**.

10. **…**.

11. **Schreibzimmer**. Auf dem Tisch ein Protokoll einer Befragung des
    Knochenmagiers Etzel zur Kontrolle der Knochengolems. („Beryl hat
    diese erstellt!“) **Truhe**: *Zauberbuch*. *Karte* → hinter dem
    Zimmer von Etzel führt eine Treppe hinab zum *Orcus Altar des
    ewigen Lebens*. *Spruchrolle der Türöffnung*. *Spruchrolle des
    Feuerballs* (5W6, Rettungswurf gegen Sprüche für die Hälfte).
    *Zaubertrank der Harpie* (8h fliegen, *Bezauberung* durch Gesang
    nach belieben, fürchterliches Aussehen, 1W6/1W6 Klauenangriff).

> **Zauberbuch des Knochenmagiers Etzel**: *Künstliches Leben* (3)
> erlaubt die Erschaffung von Golems, dessen TW unter deiner Stufe
> liegt. *Ewige Dunkelheit* (2) mit Radius 60 Fuss.
> *Aussergewöhnliches Gehör* (2) lässt dich für einen Tag alles
> innerhalb von 300 Fuss hören. Reden mehr als 10 Leute, ist dies aber
> sehr verwirrend. *Magie entdecken* (1) für 10min bis 30 Fuss.
> *Bezauberung*: Rettungswurf gegen Sprüche oder man ist dir eine
> Woche verfallen.

12. **Geheimes Gefängnis**. Hier hängt der **Knochenmagier Etzel** in
    Ketten. („Befreit mich! Ich kann euch beibringen, Golems zu
    erschaffen!“)

13. **…**.

14. **…**.

15. **…**.

16. **…**.

17. **…**.

18. **…**.

19. **…**.

20. **…**.

21. **…**.

22. **…**.

23. **…**.

24. **…**.

25. **…**.

26. **…**.

27. **…**.

28. **…**.

29. **…**.

30. **…**.

31. **…**.

32. **…**.

33. **…**.

34. **…**.

35. **…**.

36. **…**.

37. **…**.

38. **…**.

39. **…**.

40. **…**.

